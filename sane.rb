# frozen_string_literal: true

# SANE version: (16777245, 1, 0, 29)
# Available devices:[('plustek:libusb:005:046', 'Canon', 'CanoScan N670U/N676U/LiDE20', 'flatbed scanner'), ('net:192.168.2.5:plustek:libusb:005:046', 'Canon', 'CanoScan N670U/N676U/LiDE20', 'flatbed scanner')]
# Device parameters: ('color', 1, (423, 584), 8, 1269)
# Resolution 50

require 'inline'
class Sane
  inline do |builder|
    builder.add_compile_flags '-lsane'
    builder.include '<sane/sane.h>'
	builder.include '<sane/saneopts.h>'
    builder.include '<assert.h>'
    #builder.include '<stdio.h>'
    builder.prefix ' SANE_Handle hnd; SANE_Parameters parm;'
    builder.prefix '

    static int x = 215;
    static int y = 297;
    static int resolutie = 200;  
  
    typedef struct
	{
	  uint8_t *data;
	  int width;    /*WARNING: this is in bytes, get pixel width from param*/
	  int height;
	  int x;
	  int y;
	}
	Image;
'
    #builder.c_raw
    builder.c_singleton '
int init()
{
	SANE_Int version = 0;
	SANE_Status st = sane_init(&version, NULL);
	sane_init(&version, NULL);
	if (st == SANE_STATUS_GOOD)
    {
		return st;
    }
    return -1;
}
'
    builder.c '
int get_devices()
{
	const SANE_Device **devices;
	SANE_Status st;
	st = sane_get_devices(&devices, 0);
	return st;
}
'
    builder.c '
int open(char *name)
{
	//SANE_Handle hnd;
	SANE_Status sane_status = 0;
    
	if (sane_status = sane_open(name, &hnd))
	{
		printf("sane_open status: %s\n", sane_strstatus(sane_status));
	}
	//sane_start(hnd);
	return sane_status;
}
'
    builder.c '
int start()
{
	sane_start(hnd);
	return 0;
}
'
    builder.c '
int scan()
{
	SANE_Int len;
	SANE_Byte data[1024];
	sane_read(hnd, data, 1024, &len);

	//printf("%d\n", len);
	return data;
}
'
    builder.c '
int klaar()
{
	sane_exit();
	return 0;
}
'
    builder.c '
int annuleer()
{
	sane_cancel(hnd);
	return 0;
}
'
    builder.c '
int get_parameters()
{
	//SANE_Parameters parm;
	sane_get_parameters(hnd, &parm);
	printf("Parameters: format=%d last_frame=%d bytes_per_line=%d pixels_per_line=%d lines=%d depth=%d\n", parm.format, parm.last_frame, parm.bytes_per_line, parm.pixels_per_line, parm.lines, parm.depth);
	return parm.bytes_per_line * parm.lines;
}
'
    builder.c '
int sluit()
{
	sane_close(hnd);
	return 0;
}
'
 	builder.c '
void scan2()
{
    SANE_Status status;
    FILE *ofp;
    SANE_Int  len;
    SANE_Byte buffer[32 * 1024];
    SANE_Word total_bytes = 0, expected_bytes;

    total_bytes += (SANE_Word)len;
    int i, first_frame = 1, offset = 0, must_buffer = 0, hundred_percent;
    SANE_Int hang_over = -1;

    sane_get_parameters(hnd, &parm);
     int lines = parm.lines;               
     int pixels_per_line = parm.pixels_per_line;          
    
   ofp = fopen("file.pnm", "wb");

 if (first_frame)
 {
   
   fprintf(ofp, "P6\n# SANE data volgt\n%d %d\n%d\n", pixels_per_line , lines, (parm.depth <= 8) ? 255 : 65535);
    while (1)
    {
        
        double progr;
        status       = sane_read(hnd, buffer, (32 * 1024), &len);
        total_bytes += (SANE_Word)len;
        progr        = ((total_bytes * 100.) / (double)hundred_percent);

        if (progr > 100.) progr = 100.;

        if (status != SANE_STATUS_GOOD)
        {
            if (status != SANE_STATUS_EOF)
            {
                return status;
            }
            break;
        }

        if ((parm.depth != 16)) fwrite(buffer, 1, len, ofp);
        else
        {
#if !defined(WORDS_BIGENDIAN)
            int i, start = 0;

            if (hang_over > -1)
            {
                if (len > 0)
                {
                    fwrite(buffer, 1, 1, ofp);
                    buffer[0] = (SANE_Byte)hang_over;
                    hang_over = -1;
                    start     = 1;
                }
            }

/* now do the byte-swapping */
            for (i = start; i < (len - 1); i += 2)
            {
                unsigned char LSB;
                LSB           = buffer[i];
                buffer[i]     = buffer[i + 1];
                buffer[i + 1] = LSB;
            }

/* check if we have an odd number of bytes */
            if (((len - start) % 2) != 0)
            {
                hang_over = buffer[len - 1];
                len--;
            }
#endif /* if !defined(WORDS_BIGENDIAN) */

		fwrite (buffer, 1, len, ofp);
        }
    }
 }
  first_frame = 0;
    //fclose(ofp);
     fflush( ofp );
    return 0;
}'
     builder.c'
int get_optie()
{
   int value;
   SANE_Status status;
   status = sane_control_option(hnd, 3, SANE_ACTION_GET_VALUE,(void*)&value, 0);
  return value;

}'
    builder.c '
int set_option()
{
   const SANE_Option_Descriptor *opt;
   SANE_Int num_dev_options;
   SANE_Status status;
   int i;
   SANE_Int val_int;
   SANE_Int info;
   //void *optval;
   //size_t optsize;		

   info = 0x1010;

   sane_control_option (hnd, 0, SANE_ACTION_GET_VALUE, &num_dev_options, 0);
   for (i = 1; i < num_dev_options; ++i)
   {
     opt = sane_get_option_descriptor (hnd, i);
     if (opt->type == SANE_TYPE_INT || opt->type == SANE_TYPE_FIXED)
	 {
		  if(strcmp(opt->name, "br-x") == 0)
		  {
			  printf("br-x nummer: %d\n",i);
		      val_int = (215 * 65536);
		      status = sane_control_option (hnd, i, SANE_ACTION_SET_VALUE, (void *)&val_int, &info);
		    //  printf("%d\n", status);
		  }
		  if(strcmp(opt->name, "br-y") == 0)
		  {
		      printf("br-y nummer: %d\n",i);
		      val_int = (297 * 65536);
		      status = sane_control_option (hnd, i, SANE_ACTION_SET_VALUE, (void *)&val_int, &info);
		     // printf("%d\n", status);
		  }
		  if(strcmp(opt->name, "resolution") == 0)
		  {
		     printf("resolutie nummer: %d\n",i);
             val_int = resolutie;
		      status = sane_control_option (hnd, i, SANE_ACTION_SET_VALUE, (void *)&val_int, &info);
             
		  }
	   }
    }
     return status;
}'
  end
end
t = Sane.new
#puts t.get_devices
begin
 t.open('plustek:libusb:005:020')
 # t.open('brother4:bus5;dev1')
  puts "resolutie-optie 3 is #{t.get_optie}"
t.get_optie
rescue StandardError
  puts 'Helaas'
end
t.set_option
begin
  t.start
  puts t.get_parameters
rescue StandardError
  puts 'pindakaas'
end
t.scan2
t.annuleer
t.sluit
t.klaar		
